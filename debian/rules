#!/usr/bin/make -f

# see FEATURE AREAS in dpkg-buildflags(1)
export DEB_BUILD_MAINT_OPTIONS = hardening=+all,-pie

# see ENVIRONMENT in dpkg-buildflags(1)
# package maintainers to append CFLAGS
export DEB_CFLAGS_MAINT_APPEND  = -Wall -pedantic -fno-omit-frame-pointer
# package maintainers to append LDFLAGS
export DEB_LDFLAGS_MAINT_APPEND = -Wl,--as-needed

# see EXAMPLES in dpkg-buildflags(1) and read /usr/share/dpkg/*
DPKG_EXPORT_BUILDFLAGS = 1
include /usr/share/dpkg/default.mk

export ARCH=$(DEB_HOST_GNU_CPU)
export PREFIX=/usr
export MODULEDIR=/usr/lib/knot-resolver
export ETCDIR=/etc/knot-resolver

# see https://bugs.debian.org/907729
LUA_LUD_BROKEN_ARCHES := arm64
ifneq (, $(filter $(DEB_HOST_ARCH), $(LUA_LUD_BROKEN_ARCHES)))
  KRESD_EXCLUDE_ARCHES := -Nknot-resolver -Nknot-resolver-module-http
endif

%:
	dh $@ $(KRESD_EXCLUDE_ARCHES)

override_dh_auto_clean-indep:
	dh_auto_clean -- doc-clean http-clean

override_dh_auto_build-arch:
	CFLAGS="$${CFLAGS} -DNDEBUG" dh_auto_build -- V=1 BUILDMODE=static lib
	CFLAGS="$${CFLAGS} -DNDEBUG" dh_auto_build -- V=1 \
	 ROOTHINTS=/usr/share/dns/root.hints \
	 KEYFILE_DEFAULT=/usr/share/dns/root.key

override_dh_auto_build-indep:
	dh_auto_build -- V=1 http
	dh_auto_build -- V=1 doc-doxygen
	cd doc && sphinx-build -b html -D html_theme=nature . html

override_dh_auto_install-arch:
	dh_auto_install --destdir=debian/tmp -- V=1 KEYFILE_DEFAULT=/usr/share/dns/root.key
	rm -f debian/tmp/etc/knot-resolver/root.hints debian/tmp/etc/knot-resolver/icann-ca.pem

override_dh_auto_install-indep:
# install just the http/2 module
	make http-install DESTDIR=debian/tmp V=1

override_dh_installinit:
	dh_installinit -pknot-resolver --name=kresd --no-start

override_dh_installsystemd:
	dh_installsystemd -pknot-resolver --name=kresd kresd-tls.socket kresd.socket

override_dh_auto_test-indep:
override_dh_auto_test-arch:
	dh_auto_test -- V=1
ifeq (, $(filter nocheck, $(DEB_BUILD_OPTIONS)))
ifeq (, $(filter $(DEB_HOST_ARCH), $(LUA_LUD_BROKEN_ARCHES)))
	mkdir -p test-modules && (cd test-modules && ln -sf ../modules/*/*.lua ../modules/*/*.so ../modules/policy/lua-aho-corasick/*.lua ../modules/policy/lua-aho-corasick/*.so ../daemon/lua/* ./)
	MODULE_DIR=$(CURDIR)/test-modules LD_LIBRARY_PATH=$(CURDIR)/lib KRESD=$(CURDIR)/daemon/kresd debian/tests/roundtrip
endif
endif

override_dh_missing:
	dh_missing --fail-missing

override_dh_installchangelogs:
	dh_installchangelogs NEWS
